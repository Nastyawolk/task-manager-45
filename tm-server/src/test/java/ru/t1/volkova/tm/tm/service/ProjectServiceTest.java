package ru.t1.volkova.tm.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.api.service.dto.IUserDTOService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.DescriptionEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.NameEmptyException;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.repository.dto.ProjectDTORepository;
import ru.t1.volkova.tm.repository.dto.TaskDTORepository;
import ru.t1.volkova.tm.repository.dto.UserDTORepository;
import ru.t1.volkova.tm.service.*;
import ru.t1.volkova.tm.service.dto.ProjectDTOService;
import ru.t1.volkova.tm.service.dto.TaskDTOService;
import ru.t1.volkova.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class ProjectServiceTest {

    private static String USER_ID;

    private static String USER_TEST_ID;

    @Nullable
    private List<ProjectDTO> projectList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);

    @NotNull
    final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);

    @NotNull
    final IUserDTORepository userRepository = new UserDTORepository(entityManager);

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(projectRepository, connectionService);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(taskRepository, connectionService);

    @NotNull
    @Getter
    private final IUserDTOService userService =
            new UserDTOService(userRepository, connectionService, propertyService, projectService, taskService);

    @Before
    public void initRepository() {
        projectList = projectService.findAll(USER_ID);
        @Nullable final UserDTO user = userService.findByLogin("user");
        @Nullable final UserDTO userTest = userService.findByLogin("test");
        if (user == null || userTest == null || projectList == null) return;
        USER_ID = user.getId();
        USER_TEST_ID = userTest.getId();
    }

    @Test
    public void testCreate(
    ) {
        @Nullable final ProjectDTO project = projectService.create(USER_ID, "new_project", "new description");
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(USER_ID, project.getId()));
    }

    @Test
    public void testCreateNegative(
    ) {
        @Nullable final ProjectDTO project = projectService.create(null, "new_project", "new description");
        Assert.assertNull(project);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) {
        projectService.create(USER_ID, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) {
        projectService.create(USER_TEST_ID, "new", null);
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        if (projectList == null) return;
        @Nullable final ProjectDTO project = projectList.get(0);
        if (project == null) return;
        projectService.updateById(project.getUserId(), project.getId(), newName, newDescription);
        @Nullable final ProjectDTO newProject = projectService.findOneById(project.getUserId(), project.getId());
        if (newProject == null) return;
        Assert.assertEquals(newName, newProject.getName());
        Assert.assertEquals(newDescription, newProject.getDescription());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateNotFoundProject(
    ) {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final String id = "non-existent-id";
        projectService.updateById(USER_ID, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) {
        projectService.updateById(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) {
        projectService.updateById(USER_ID, projectList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) {
        projectService.updateById(USER_TEST_ID, projectList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        @Nullable final ProjectDTO project = projectService.updateByIndex(USER_ID, index, newName, newDescription);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) {
        projectService.updateByIndex(USER_ID, 10, "name", "new description");
        projectService.updateByIndex(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) {
        projectService.updateByIndex(USER_ID, 2, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        projectService.updateByIndex(USER_ID, index, "name", null);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final ProjectDTO projectToUpdate = projectList.get(0);
        @Nullable final ProjectDTO project = projectService.changeProjectStatusById(projectToUpdate.getUserId(), projectToUpdate.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() {
        projectService.changeProjectStatusById(USER_ID, null, Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "", Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundById() {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() {
        @Nullable final ProjectDTO project = projectService.changeProjectStatusByIndex(USER_ID, 2, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() {
        projectService.changeProjectStatusByIndex(USER_ID, 22, Status.IN_PROGRESS);
        projectService.changeProjectStatusByIndex(USER_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundByIndex() {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        final int index2 = random.nextInt(projectService.getSize(USER_ID));
        Assert.assertNotNull(projectService.findOneById(USER_ID, projectList.get(index).getId()));
        Assert.assertNotNull(projectService.findOneById(USER_TEST_ID, projectList.get(index2).getId()));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFindOneByIdNegative() {
        Assert.assertNotNull(projectService.findOneById(USER_ID, "non-existent"));
    }

}
