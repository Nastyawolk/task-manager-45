package ru.t1.volkova.tm.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.repository.dto.ProjectDTORepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull ProjectDTO project = new ProjectDTO();
            project.setName("Rproject" + i);
            project.setDescription("description" + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddForUserId() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2 + 1;
        @NotNull final String name = "Test project";
        @NotNull final String description = "Test description";
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_1));
    }

    @Test
    public void testFindAll() {
        @Nullable final List<ProjectDTO> projectList = projectRepository.findAll();
    }

    @Test
    public void testFindAllForUser() {
        @Nullable final List<ProjectDTO> projectList = projectRepository.findAll(USER_ID_1);
        @Nullable final List<ProjectDTO> projectList2 = projectRepository.findAll(USER_ID_2);
        if (projectList != null && projectList2 != null) {
            assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectList.size());
            assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        @Nullable final List<ProjectDTO> projectList = projectRepository.findAll((String) null);
        assertEquals(0, projectList.size());
        @Nullable final List<ProjectDTO> projectList2 = projectRepository.findAll("non-existent-id");
        if (projectList2 == null) return;
        assertEquals(0, projectList2.size());
    }

    @Test
    public void testFindOneByIdForUser() {
        for (@NotNull final ProjectDTO project : projectList) {
            assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertNull(projectRepository.findOneById(null, projectList.get(1).getId()));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, "NotExcitingId"));
        Assert.assertNull(projectRepository.findOneById(USER_ID_2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @Nullable final ProjectDTO project1 = projectRepository.findOneByIndex(USER_ID_1, 1);
        @NotNull final ProjectDTO expected1 = projectList.get(1);
        assertEquals(expected1, project1);
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        Assert.assertNull(projectRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES + 10));
    }

    @Test
    public void testRemoveOneForUser() {
        @Nullable final ProjectDTO project1 = projectRepository.findOneById(USER_ID_1, projectList.get(0).getId());
        projectRepository.removeOneById(project1.getId());
        assertNull(projectRepository.findOneById(USER_ID_1, projectList.get(0).getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final ProjectDTO project1 = projectList.get(i);
            projectRepository.removeOneById(USER_ID_1, project1.getId());
            assertNull(projectRepository.findOneById(USER_ID_1, project1.getId()));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project2 = projectList.get(i);
            projectRepository.removeOneById(USER_ID_2, project2.getId());
            assertNull(projectRepository.findOneById(USER_ID_2, project2.getId()));
        }
    }

    @Test
    public void testRemoveAllForUser() {
        projectRepository.clear(USER_ID_1);
        projectRepository.clear(USER_ID_2);
        assertEquals(0, projectRepository.getSize(USER_ID_1));
        assertEquals(0, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() {
        projectRepository.clear("NotExcitingId");
        projectRepository.clear((String) null);
        Assert.assertNotEquals(0, projectRepository.getSize(USER_ID_1));
        Assert.assertNotEquals(0, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @Nullable final ProjectDTO project1 = projectRepository.findOneByIndex(USER_ID_1, 1);
        projectRepository.removeOneByIndex(USER_ID_1, 1);
        assertEquals(NUMBER_OF_ENTRIES / 2, projectRepository.getSize(USER_ID_1));
        if (project1 != null) Assert.assertNull(projectRepository.findOneById(USER_ID_1, project1.getId()));
    }

    @Test
    public void testGetSizeForUser() {
        assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSize(USER_ID_1));
        assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectRepository.getSize(USER_ID_2));
        assertEquals(projectList.size() / 2 + 1, projectRepository.getSize(USER_ID_1));
        assertEquals(projectList.size() / 2 - 1, projectRepository.getSize(USER_ID_2));
    }

}
