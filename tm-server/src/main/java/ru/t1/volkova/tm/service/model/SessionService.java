package ru.t1.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.model.ISessionRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.model.ISessionService;
import ru.t1.volkova.tm.exception.entity.SessionNotFoundException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;
import ru.t1.volkova.tm.model.Session;
import ru.t1.volkova.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    public SessionService(
            @NotNull final ISessionRepository repository,
            @NotNull final IConnectionService connectionService) {
        super(repository, connectionService);
    }

    @Override
    public void add(@NotNull final Session entity) {
        super.add(entity);
    }

    @NotNull
    @Override
    public Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            @Nullable final Session session = repository.findOneById(userId, id);
            if (session == null) throw new SessionNotFoundException();
            return session;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Session findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            @Nullable final Session session = repository.findOneByIndex(userId, index);
            if (session == null) throw new SessionNotFoundException();
            return session;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            @Nullable final List<Session> sessions = repository.findAll(userId);
            if (sessions == null) throw new SessionNotFoundException();
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            @Nullable final List<Session> sessions = repository.findAll(userId);
            if (sessions == null) throw new SessionNotFoundException();
            return sessions;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId,
                                 @NotNull final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            @Nullable final List<Session> sessions = repository.findAll(userId, comparator);
            if (sessions == null) throw new SessionNotFoundException();
            return sessions;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            findOneById(userId, id);
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            findOneByIndex(userId, index);
            repository.removeOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


}
