package ru.t1.volkova.tm.api.service.dto;

import ru.t1.volkova.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedDTOModel;

@SuppressWarnings("UnusedReturnValue")
public interface IUserOwnedDTOService<M extends AbstractUserOwnedDTOModel> extends IAbstractUserOwnedDTORepository<M> {


}
