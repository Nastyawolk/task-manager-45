package ru.t1.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface IProjectDTOService {

    @NotNull IProjectDTORepository getRepository();

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    ProjectDTO changeProjectStatusByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @Nullable
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    void clear(@Nullable String userId);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @NotNull Comparator comparator);

    int getSize(@Nullable String userId);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index);

}
