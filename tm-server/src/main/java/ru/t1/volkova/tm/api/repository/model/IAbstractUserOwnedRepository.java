package ru.t1.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(
            @Nullable String userId,
            @NotNull Comparator<M> comparator
    );

    M findOneById(@Nullable String userId, @Nullable String id);

    M findOneByIndex(@Nullable String userId, @NotNull Integer index);

    int getSize(@Nullable String userId);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@Nullable String userId, @NotNull Integer index);

    void clear(@Nullable String userId);

}
