package ru.t1.volkova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.service.IServiceLocator;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.user.AccessDeniedException;
import ru.t1.volkova.tm.dto.model.SessionDTO;

@NoArgsConstructor
public abstract class AbstractEndPoint {

    @NotNull
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    @Getter
    protected IServiceLocator serviceLocator;

    public AbstractEndPoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
