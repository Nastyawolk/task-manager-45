package ru.t1.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Update project by id.";

    @NotNull
    private static final String NAME = "project-update-by-id";

    @Override
    public void execute() throws SQLException {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().updateProjectById(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
