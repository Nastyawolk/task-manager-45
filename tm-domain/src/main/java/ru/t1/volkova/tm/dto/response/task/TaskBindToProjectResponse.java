package ru.t1.volkova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.TaskDTO;

@NoArgsConstructor
public class TaskBindToProjectResponse extends AbstractTaskResponse {

    public TaskBindToProjectResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
