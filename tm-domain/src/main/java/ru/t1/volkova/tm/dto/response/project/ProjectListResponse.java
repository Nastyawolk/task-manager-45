package ru.t1.volkova.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractResponse;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
